package api

type StockService interface {
	SearchStock(keyword string) []*Stock
	GetByCode(code string) *Stock
	BatchGetByCode(codes []string) map[string]*Stock
}
