package api

import "time"

const (
	STOCK_TYPE_SH  = "SH"  // 沪市
	STOCK_TYPE_SZ  = "SZ"  // 深市
	STOCK_TYPE_HK  = "HK"  // 港市
	STOCK_TYPE_JJ  = "JJ"  // 基金(场外)
	STOCK_TYPE_CSI = "CSI" // 沪深指数
	STOCK_TYPE_HKI = "HKI" // 港股指数

	CODE_SUFFIX_SEPARATOR = "." // 标准Code code和后缀的分隔符
)

type Stock struct {
	Code       string // 标准Code
	Name       string
	Price      string
	UpdateTime *time.Time
}

type StockHistory struct {
	Code  string // 标准Code
	Name  string
	Date  string // 日期
	Open  string
	Close string
	Low   string
	High  string
}
