package main

import (
	"flag"
	"fmt"

	"gitee.com/lenchu/stock-api-go/api"
	"gitee.com/lenchu/stock-api-go/eastmoney"
	"gitee.com/lenchu/stock-api-go/xueqiu"
	"gitee.com/lenchu/stock-api-go/xueqiu2"
)

const (
	API_PROVIDER_XUEQIU    = "xueqiu"
	API_PROVIDER_XUEQIU_2  = "xueqiu2"
	API_PROVIDER_EASTMONEY = "eastmoney"

	ACTION_SEARCH            = "search"
	ACTION_GET_BY_CODE       = "getByCode"
	ACTION_BATCH_GET_BY_CODE = "batchGetByCode"
)

func NewStockService(provider string) (service api.StockService) {
	switch provider {
	case API_PROVIDER_XUEQIU:
		service = &xueqiu.StockService{}
	case API_PROVIDER_EASTMONEY:
		service = &eastmoney.StockService{}
	case API_PROVIDER_XUEQIU_2:
		service = xueqiu2.NewStockService(nil)
	}
	if service == nil {
		service = &xueqiu.StockService{}
	}
	return service
}

func main() {
	runCmd()
}

func runCmd() {
	provider := flag.String("p", "xueqiu", "api provider, 可选项有: xueqiu")
	action := flag.String("a", "search", "action, 可选项有: search, getByCode, batchGetByCode")
	flag.Parse()

	service := NewStockService(*provider)
	switch *action {
	case ACTION_SEARCH:
		result := service.SearchStock(flag.Args()[0])
		for _, r := range result {
			fmt.Println(*r)
		}
	case ACTION_GET_BY_CODE:
		result := service.GetByCode(flag.Args()[0])
		fmt.Println(*result)
	case ACTION_BATCH_GET_BY_CODE:
		result := service.BatchGetByCode(flag.Args())
		for _, r := range result {
			fmt.Println(*r)
		}
	}
}
