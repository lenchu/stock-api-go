package utils

import "strings"

func SubStringBetween(str string, startStr string, endStr string) (
	result string,
	foundStart bool,
	foundEnd bool,
) {
	_, after, found := strings.Cut(str, startStr)
	if !found {
		return "", false, false
	}
	before2, _, found2 := strings.Cut(after, endStr)
	if !found2 {
		return "", true, false
	}
	return before2, true, true
}
