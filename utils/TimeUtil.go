package utils

import (
	"strconv"
	"time"
)

const timeFormat = "2006-01-02 15:04:05"

func TimestampToTime(timestamp string) *time.Time {
	i, _ := strconv.ParseInt(timestamp, 10, 64)
	t := time.UnixMilli(i)
	return &t
}
func StringToTime(timeStr string) *time.Time {
	t, err := time.Parse(timeFormat, timeStr)
	if err != nil {
		return nil
	}
	return &t
}

func FormatTime(t *time.Time) string {
	return t.Format(timeFormat)
}

func FormatTimestamp(timestamp string) string {
	t := TimestampToTime(timestamp)
	return FormatTime(t)
}
