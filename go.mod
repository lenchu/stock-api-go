module gitee.com/lenchu/stock-api-go

go 1.19

require (
	github.com/gookit/goutil v0.5.11 // direct
	github.com/tidwall/gjson v1.14.3 // direct
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)
