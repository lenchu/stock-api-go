package eastmoney

import (
	"io/ioutil"

	"gitee.com/lenchu/stock-api-go/utils"
)

func (this *StockService) requestToken() string {
	client := this.getHttpClient()
	resp, err := client.Get(request_token_url)
	if err != nil {
		return ""
	}
	byteBody, err := ioutil.ReadAll(resp.Body)
	strBody := string(byteBody)
	result, foundStart, foundEnd := utils.SubStringBetween(strBody, token_prefix, token_suffix)
	if foundStart && foundEnd {
		return result
	}
	return ""
}
