package eastmoney

import (
	logger "log"
)

const (
	logger_prefix = "[Eastmoney] "

	request_token_url = "https://emcharts.dfcfw.com/suggest/stocksuggest2017.min.js"
	token_prefix      = "token:\""
	token_suffix      = "\""

	search_url                = "https://searchapi.eastmoney.com/api/suggest/get?type=14&input="
	batch_get_by_code_url     = "http://push2.eastmoney.com/api/qt/ulist/get?fields=f1,f2,f12,f13,f14&pn=1&secids="
	MARKET_NUM_CODE_SEPARATOR = "."

	CHI_PREFIX_SH = "000"
	CHI_PREFIX_SZ = "399"
)

var log = logger.New(logger.Writer(), logger_prefix, logger.Flags())
